let getOperands,
  getOperation,
  getOperand,
  result = 0;

do {
  getOperation = prompt("Choose an operation (+ - / *)", "+");
} while (getOperation !== "+" && getOperation !== "-" && getOperation !== "/" && getOperation !== "*");

do {
  getOperands = parseInt(prompt("How many operands do you want?", "2"));
} while (isNaN(getOperands) || getOperands <= 1 || getOperands > 7 || getOperands === null);

switch (getOperation) {
  case "+":
    for (let i = 1; i <= getOperands; i++) {
      do {
        getOperand = parseInt(prompt(`Set your ${i} number`, "5"));
      } while (getOperand !== getOperand);

      result += getOperand;
    }
    break;
  case "-":
    for (let i = 1; i <= getOperands; i++) {
      do {
        getOperand = parseInt(prompt(`Set your ${i} number`, "5"));
      } while (getOperand !== getOperand);

      result = i === 1 ? (result = getOperand) : (result -= getOperand);
    }
    break;
  case "*":
    for (let i = 1; i <= getOperands; i++) {
      do {
        getOperand = parseInt(prompt(`set your ${i} number`, "5"));
      } while (getOperand !== getOperand);

      result = i === 1 ? (result = getOperand) : (result *= getOperand);
    }
    break;
  case "/":
    for (let i = 1; i <= getOperands; i++) {
      do {
        getOperand = parseInt(prompt(`Set your ${i} number`, "5"));
        if (getOperand === 0) console.log("Can't be divided by 0");
      } while (getOperand !== getOperand || getOperand === 0);

      result = i === 1 ? (result = getOperand) : (result /= getOperand);
    }
    break;
}

console.log(`Your result is${result}`);
